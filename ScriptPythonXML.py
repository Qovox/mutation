import os
import shutil
import subprocess
import webbrowser
import xml.etree.ElementTree

###------------------------------------------------------------------------------
###                               Useful variables
###------------------------------------------------------------------------------

# Needed directories
binDirectory = 'bin/'
libDirectory = 'lib/'
reportDirectory = 'reports/'
directories = [binDirectory, libDirectory, reportDirectory]

# Paths
packagePath = 'fr/unice/polytech/processors/'
packagePathDot= 'fr.unice.polytech.processors.'
projectPath = 'project/'
src = 'mutation/'
srcSubPath = 'src/main/java/' + packagePath

###------------------------------------------------------------------------------
###                               Script functions
###------------------------------------------------------------------------------

## Execute a command
## Param: [command:str] the command to execute
def executeCommand(command):
    print('Executing command \'' + command + '\' ...')
    subprocess.call(command, shell=True)
    print('\'' + command + '\' successfully executed!\n')

###------------------------------------------------------------------------------
###                             Useful functions
###------------------------------------------------------------------------------      

## Create a list of directories
## Param: [directories:list]
def createDirectories(directories):
    for directory in directories:
        if not os.path.exists(directory):
            print('Creating directory ' + directory + ' ...')
            os.makedirs(directory)
            print(directory + ' successfully created!\n')

## Remove directory from current directory to the target directory
## Param: [directory:str] the name of the directory to remove
def removeDirectory(directory):
    if (os.path.exists(directory)):
        print('Removing ' + directory + ' ...')
        shutil.rmtree(directory)
        print(directory + ' successfully removed!\n')

## Move file from current directory to the target directory
## Param: [file:str] the name of the file to move
##        [targetDirectory:str] the target directory
##        [extension:str] the file extension if needed
def moveFile(file, targetDirectory, extension = ''):
    if (os.path.exists(file + extension)):
        print('Copying ' + file + extension + ' to ' + os.path.realpath(targetDirectory) + ' ...')
        shutil.copy(file + extension, targetDirectory)
        print(file + extension + ' successfully copied!\n')
        print('Removing ' + file + extension + ' ...')
        os.remove(file + extension)
        print(file + extension + ' successfully removed!\n')

## Read files by extension
## Param: [path:str] the path to directory in which read files
##        [extension:str] the extension of the files
def readFilesByExtension(path, extension):
    text = ''
    files = os.listdir(path)
    for file in files:
        with open(path + str(file)) as fp:
            for line in fp:
                text += line
    return text

## Write a text in a new file
## Param: [file:str] the file name and path
##        [text:str] the text to write in the file
def writeTextInFile(file, text):
    f = open(file, 'w')
    f.write(text)
    f.close()

## Open a file in the broswer
## Param: [file:str] the name of the file to open in the browser
def openFileInBrowser(file):
    print('Opening ' + file + ' in browser ...')
    webbrowser.open('file://' + os.path.realpath(file));
    print(file + ' successfully opened!\n')

###------------------------------------------------------------------------------
###                             Script code
###------------------------------------------------------------------------------

# Remove reports directory if it already exists
removeDirectory(reportDirectory)

# Create useful directories to create a clean project
createDirectories(directories)

# List the different mutators (processors)
mutators = os.listdir(src + srcSubPath)
print(str(len(mutators)) + ' mutators (Processors) found\n')

currentDirectory = os.getcwd()

xml.etree.ElementTree.register_namespace('', 'http://maven.apache.org/POM/4.0.0')
xml.etree.ElementTree.register_namespace('xsi', 'http://www.w3.org/2001/XMLSchema-instance')

executeCommand('mvn -f mutation/pom.xml clean package install')

for mutator in mutators:
    # Parse pom.xml
    mutatorName = str(mutator).split('.')[0]

    tree = xml.etree.ElementTree.parse('project/pom.xml')
    root = tree.getroot()

    for pluginElement in root.findall('{http://maven.apache.org/POM/4.0.0}build/' +
        '{http://maven.apache.org/POM/4.0.0}plugins/' +
        '{http://maven.apache.org/POM/4.0.0}plugin'):
        if (pluginElement.find('{http://maven.apache.org/POM/4.0.0}groupId').text == 'fr.inria.gforge.spoon'):
            # Remove configuration element to ensure recreating it well
            if (pluginElement.find('{http://maven.apache.org/POM/4.0.0}configuration') is not None):
                pluginElement.remove(pluginElement.find('{http://maven.apache.org/POM/4.0.0}configuration'))
            configurationElement = xml.etree.ElementTree.SubElement(pluginElement, 'configuration')
            processorsElement = xml.etree.ElementTree.SubElement(configurationElement, 'processors')
            processorElement = xml.etree.ElementTree.SubElement(processorsElement, 'processor')
            processorElement.text = packagePathDot + mutatorName

    tree.write('project/pom.xml')

    # Run maven and surefire-report, open reports in web browser
    executeCommand('mvn -f project/pom.xml clean package install test')
    executeCommand('mvn -f project/pom.xml surefire-report:report')
    moveFile('project/target/site/surefire-report.html', reportDirectory + mutatorName + '-report.html')
    #openFileInBrowser(reportDirectory + mutatorName + '-report.html')
