package fr.unice.polytech.cookiefactory15.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import fr.unice.polytech.cookiefactory15.TheCookieFactory;
import fr.unice.polytech.cookiefactory15.cookieparts.*;
import fr.unice.polytech.cookiefactory15.orderables.Cookie;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ManageMarginsStepdefs {

    private TheCookieFactory tcf;
    private Cookie customCookie;

    @Given("^the user of the software who wants to change the different margins and a custom cookie$")
    public void theUserOfTheSoftwareWhoWantsToChangeTheDifferentMarginsAndACustomCookie() throws Throwable {
        TheCookieFactory.get_instance().reset();
        tcf = TheCookieFactory.get_instance();
        customCookie = new Cookie.CookieBuilder()
                .withCooking(Cooking.CHEWY)
                .withDough(Dough.PEANUT_BUTTER)
                .withFlavour(Flavour.CHILI)
                .withMix(Mix.MIXED)
                .withTopping(Topping.MILK_CHOCO)
                .build();
    }

    @When("^the user sets the margin of the dough \"([^\"]*)\" to \"([^\"]*)\" dollar$")
    public void theUserSetsTheMarginOfTheDoughToDollar(String arg0, String arg1) throws Throwable {
        Dough.valueOf(arg0).setMargin(Double.parseDouble(arg1));
    }

    @Then("^the margin of the dough \"([^\"]*)\" is \"([^\"]*)\" dollar$")
    public void theMarginOfTheDoughIsDollar(String arg0, String arg1) throws Throwable {
        assertEquals(Double.parseDouble(arg1), Dough.valueOf(arg0).getMargin());
    }

    @And("^the price of the dough \"([^\"]*)\" is \"([^\"]*)\" dollar$")
    public void thePriceOfTheDoughIsDollar(String arg0, String arg1) throws Throwable {
        double price = Math.floor(Dough.valueOf(arg0).getPrice() * 100) / 100;
        assertEquals(Double.parseDouble(arg1), price);
    }

    @When("^the user sets the margin to \"([^\"]*)\" dollar of a custom cookies whose total price is \"([^\"]*)\" dollars$")
    public void theUserSetsTheMarginToDollarOfACustomCookiesWhichCostsDollars(String arg0, String arg1) throws Throwable {
        double price = Math.floor(customCookie.calculateTotalPrice() * 100) / 100;
        assertEquals(Double.parseDouble(arg1), price);
        tcf.setCustomCookieMargin(Double.parseDouble(arg0));
    }

    @Then("^the margin of custom cookies is \"([^\"]*)\" dollar$")
    public void theMarginOfCustomCookiesIsDollar(String arg0) throws Throwable {
        assertEquals(Double.parseDouble(arg0), TheCookieFactory.deltaPrice);
    }

    @And("^the price of the cookie is now \"([^\"]*)\" dollars$")
    public void thePriceOfTheCookieIsNowDollars(String arg0) throws Throwable {
        //Be careful Math.floor will round the value down
        double price = Math.floor(customCookie.calculateTotalPrice() * 100) / 100;
        assertEquals(Double.parseDouble(arg0), price);
        tcf.setCustomCookieMargin(0.05);
    }

}
