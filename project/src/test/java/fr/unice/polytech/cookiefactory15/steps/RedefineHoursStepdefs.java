package fr.unice.polytech.cookiefactory15.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import fr.unice.polytech.cookiefactory15.Manager;
import fr.unice.polytech.cookiefactory15.Shop;
import fr.unice.polytech.cookiefactory15.TheCookieFactory;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RedefineHoursStepdefs {

    private TheCookieFactory theCookieFactory = TheCookieFactory.get_instance();

    @Given("^a manager named \"([^\"]*)\" working at \"([^\"]*)\"$")
    public void aManagerNamedWorkingAt(String arg0, String arg1) throws Throwable {
        Shop shop = new Shop(arg1);
        Manager manager = new Manager(arg0);
        shop.addManager(manager);
        theCookieFactory.addShop(shop);
    }

    @When("^\"([^\"]*)\" redefine open hour of \"([^\"]*)\" to \"([^\"]*)\"$")
    public void redefineOpenHourOfTo(String arg0, String arg1, String arg2) throws Throwable {
        if (theCookieFactory.getShopByName(arg1).isPresent()) {
            Shop shop = theCookieFactory.getShopByName(arg1).get();
            if (shop.getManagerByName(arg0).isPresent()) {
                Manager manager = shop.getManagerByName(arg0).get();
                shop.changeHour(manager, arg2, true);
            }
        }
    }

    @Then("^shop \"([^\"]*)\" open hour is \"([^\"]*)\"$")
    public void shopOpenHourIs(String arg0, String arg1) throws Throwable {
        if (theCookieFactory.getShopByName(arg0).isPresent()) {
            Shop shop = theCookieFactory.getShopByName(arg0).get();
            assertEquals(arg1, shop.getHourToString(true));
        }
    }

    @When("^\"([^\"]*)\" redefine close hour of \"([^\"]*)\" to \"([^\"]*)\"$")
    public void redefineCloseHourOfTo(String arg0, String arg1, String arg2) throws Throwable {
        if (theCookieFactory.getShopByName(arg1).isPresent()) {
            Shop shop = theCookieFactory.getShopByName(arg1).get();
            if (shop.getManagerByName(arg0).isPresent()) {
                Manager manager = shop.getManagerByName(arg0).get();
                shop.changeHour(manager, arg2, false);
            }
        }
    }

    @Then("^shop \"([^\"]*)\" close hour is \"([^\"]*)\"$")
    public void shopCloseHourIs(String arg0, String arg1) throws Throwable {
        if (theCookieFactory.getShopByName(arg0).isPresent()) {
            Shop shop = theCookieFactory.getShopByName(arg0).get();
            assertEquals(arg1, shop.getHourToString(false));
        }
    }

}
