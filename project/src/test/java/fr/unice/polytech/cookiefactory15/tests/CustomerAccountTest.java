package fr.unice.polytech.cookiefactory15.tests;

import fr.unice.polytech.cookiefactory15.customers.CustomerAccount;
import fr.unice.polytech.cookiefactory15.stateOrder.OrderPayedSaved;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.powermock.api.mockito.PowerMockito.when;

@PrepareForTest(CustomerAccount.class)
@RunWith(PowerMockRunner.class)
public class CustomerAccountTest {

    private CustomerAccount customerAccount;

    @Before
    public void setUp() {
        customerAccount = new CustomerAccount();
        customerAccount.addVoucher(LocalDateTime.of(2017, 11, 5, 18, 21), 12.58);
        customerAccount.addVoucher(LocalDateTime.of(2017, 12, 15, 14, 50), 25.69);
        customerAccount.addVoucher(LocalDateTime.of(2017, 10, 12, 10, 2), 15.0);
    }

    @Test
    public void testAddVouchers() {
        assertEquals(3, customerAccount.getVouchers().size());
    }

    @Test
    public void testLastVoucher() {
        assertEquals(15.0, customerAccount.lastVoucher(), 0.01);
        assertEquals(2, customerAccount.getVouchers().size());
    }

    @Test
    public void testCheckVoucher() {
        LocalDateTime mockDate= LocalDateTime.parse("2017-12-15 14:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        PowerMockito.mockStatic(LocalDateTime.class);
        when(LocalDateTime.now()).thenReturn(mockDate);
        customerAccount.addVoucher(LocalDateTime.of(2017, 5, 25, 22, 44), 21.5);
        assertEquals(4, customerAccount.getVouchers().size());
        customerAccount.checkVoucher();
        assertEquals(2, customerAccount.getVouchers().size());
    }

}
