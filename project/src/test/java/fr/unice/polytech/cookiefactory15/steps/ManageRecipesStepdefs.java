package fr.unice.polytech.cookiefactory15.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import fr.unice.polytech.cookiefactory15.TheCookieFactory;
import fr.unice.polytech.cookiefactory15.cookieparts.*;
import fr.unice.polytech.cookiefactory15.orderables.Cookie;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ManageRecipesStepdefs {

    private Cookie darkTemptation = new Cookie.CookieBuilder()
            .withName("Dark Temptation")
            .withCooking(Cooking.CHEWY)
            .withDough(Dough.OATMEAL)
            .withFlavour(Flavour.CHILI)
            .withMix(Mix.TOPPED)
            .withTopping(Topping.M_AND_M)
            .build();

    private Cookie marcelsFavorite = new Cookie.CookieBuilder()
            .withName("Marcel's favorite")
            .withCooking(Cooking.CRUNCHY)
            .withDough(Dough.CHOCO)
            .withFlavour(Flavour.CINNAMON)
            .withMix(Mix.MIXED)
            .withTopping(Topping.REESE_BUTTERCUP)
            .build();

    @Given("^A shop with a known recipe Dark Temptation$")
    public void aShopWithAKnownRecipeDarkTemptation() throws Throwable {
        TheCookieFactory.get_instance().reset(); // we reset tcf so that each test is isolated
        TheCookieFactory.get_instance().addCookieRecipe(darkTemptation);
    }

    @When("^The marketing team adds the recipe Marcel's Favorite$")
    public void theMarketingTeamAddsTheRecipeMarcelSFavorite() throws Throwable {
        TheCookieFactory.get_instance().addCookieRecipe(marcelsFavorite);
    }

    @Then("^The recipe Dark Temptation is a known recipe$")
    public void theRecipeDarkTemptationIsAKnownRecipe() throws Throwable {
        assertTrue(TheCookieFactory.get_instance().isAKnownRecipe(darkTemptation));
    }

    @And("^The recipe Marcel's Favorite is a known recipe$")
    public void theRecipeMarcelSFavoriteIsAKnownRecipe() throws Throwable {
        assertTrue(TheCookieFactory.get_instance().isAKnownRecipe(marcelsFavorite));
    }

    @When("^The marketing team removes the recipe Dark Temptation$")
    public void theMarketingTeamRemoveTheRecipeDarkTemptation() throws Throwable {
        TheCookieFactory.get_instance().removeCookieRecipe(darkTemptation);
    }

    @Then("^The recipe Dark Temptation is not a known recipe$")
    public void theRecipeDarkTemptationIsNotAKnownRecipe() throws Throwable {
        assertFalse(TheCookieFactory.get_instance().isAKnownRecipe(darkTemptation));
    }


    @And("^The recipe Marcel's Favorite is not a known recipe$")
    public void theRecipeMarcelSFavoriteIsNotAKnownRecipe() throws Throwable {
        assertFalse(TheCookieFactory.get_instance().isAKnownRecipe(marcelsFavorite));
    }

}
