package fr.unice.polytech.cookiefactory15.tests;

import fr.unice.polytech.cookiefactory15.Stats;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

public class StatsTest {

    private Stats stats;

    @Before
    public void setUp() {
        stats = new Stats();
    }

    @Test
    public void statsPrint() {
        stats.newOrder(LocalDateTime.of(2017, 12, 12, 10, 0));
        assertEquals("Orders pickup at 10:0 : 1\n", stats.print());
    }

    @Test
    public void getOrderByHour() {
        assertEquals(0, stats.getOrderByHour("11:45"));
        stats.newOrder(LocalDateTime.of(2017, 12, 12, 11, 45));
        assertEquals(1, stats.getOrderByHour("11:45"));
    }

}
