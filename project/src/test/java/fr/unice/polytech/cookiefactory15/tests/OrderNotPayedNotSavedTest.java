package fr.unice.polytech.cookiefactory15.tests;

import fr.unice.polytech.cookiefactory15.Stock;
import fr.unice.polytech.cookiefactory15.cookieparts.*;
import fr.unice.polytech.cookiefactory15.customers.Customer;
import fr.unice.polytech.cookiefactory15.orderables.Cookie;
import fr.unice.polytech.cookiefactory15.orderables.Orderable;
import fr.unice.polytech.cookiefactory15.stateOrder.OrderCanceled;
import fr.unice.polytech.cookiefactory15.stateOrder.OrderNotPayedNotSaved;
import fr.unice.polytech.cookiefactory15.stateOrder.OrderPayedSaved;
import fr.unice.polytech.cookiefactory15.stateOrder.StateOrder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.when;

@PrepareForTest(OrderNotPayedNotSaved.class)
@RunWith(PowerMockRunner.class)

public class OrderNotPayedNotSavedTest {

    private StateOrder stateOrder;
    private Customer client;
    private Stock stock = new Stock();
    private Map<Orderable, Integer> cookies_to_order;

    @Before
    public void init() {
        this.stateOrder = new OrderNotPayedNotSaved();
        this.client = new Customer("Marcel");
        client.createAccount();
        this.client.getCustomerAccount().becomePremium();

        stock.addToTopping(Topping.WHITE_CHOCO, 100);
        stock.addToTopping(Topping.MILK_CHOCO, 100);
        stock.addToTopping(Topping.M_AND_M, 100);
        stock.addToTopping(Topping.REESE_BUTTERCUP, 100);
        stock.addToFlavour(Flavour.VANILLA, 100);
        stock.addToFlavour(Flavour.CHILI, 100);
        stock.addToFlavour(Flavour.CINNAMON, 100);
        stock.addToDough(Dough.CHOCO, 100);
        stock.addToDough(Dough.PLAIN, 100);
        stock.addToDough(Dough.OATMEAL, 100);
        stock.addToDough(Dough.PEANUT_BUTTER, 100);

        this.cookies_to_order = new HashMap<>();
        Orderable cookie1 = new Cookie.CookieBuilder()
                .withCooking(Cooking.CRUNCHY)
                .withDough(Dough.OATMEAL)
                .withFlavour(Flavour.CINNAMON)
                .withMix(Mix.TOPPED).build();
        Orderable cookie2 = new Cookie.CookieBuilder()
                .withCooking(Cooking.CHEWY)
                .withDough(Dough.OATMEAL)
                .withFlavour(Flavour.VANILLA)
                .withMix(Mix.MIXED).build();

        cookies_to_order.put(cookie1, 50);
        cookies_to_order.put(cookie2, 50);


    }

    @Test
    public void payTestWithOrderDoable() throws Exception {
        assertEquals(OrderPayedSaved.class, stateOrder.pay(cookies_to_order, stock).getClass());
        assertEquals(50, stock.getFlavourIngredientAmount(Flavour.VANILLA));
    }

    @Test
    public void deleteCookiesTest() throws Exception {
        LocalDateTime mockDate = LocalDateTime.parse("2017-12-01 14:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        LocalDateTime dateOrder = LocalDateTime.parse("2017-12-01 17:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        PowerMockito.mockStatic(LocalDateTime.class);
        when(LocalDateTime.now()).thenReturn(mockDate);

        Map<Orderable, Integer> cookies_to_delete = new HashMap<>();
        Orderable cookie = new Cookie.CookieBuilder()
                .withCooking(Cooking.CHEWY)
                .withDough(Dough.OATMEAL)
                .withFlavour(Flavour.VANILLA)
                .withMix(Mix.MIXED).build();
        cookies_to_delete.put(cookie, 40);

        assertEquals(OrderNotPayedNotSaved.class, stateOrder.deleteCookies(cookies_to_order, cookies_to_delete, dateOrder, client, stock).getClass());
        assertEquals(10, cookies_to_order.get(cookie).intValue());
    }

    @Test
    public void deleteCookiesTestCancelingOrder() throws Exception {
        LocalDateTime mockDate = LocalDateTime.parse("2017-12-01 14:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        LocalDateTime dateOrder = LocalDateTime.parse("2017-12-01 17:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        PowerMockito.mockStatic(LocalDateTime.class);
        when(LocalDateTime.now()).thenReturn(mockDate);

        Map<Orderable, Integer> cookies_to_delete = new HashMap<>();
        Orderable cookie1 = new Cookie.CookieBuilder()
                .withCooking(Cooking.CHEWY)
                .withDough(Dough.OATMEAL)
                .withFlavour(Flavour.VANILLA)
                .withMix(Mix.MIXED).build();
        Orderable cookie2 = new Cookie.CookieBuilder()
                .withCooking(Cooking.CRUNCHY)
                .withDough(Dough.OATMEAL)
                .withFlavour(Flavour.CINNAMON)
                .withMix(Mix.TOPPED).build();
        cookies_to_delete.put(cookie1, 50);
        cookies_to_delete.put(cookie2, 50);


        assertEquals(OrderCanceled.class, stateOrder.deleteCookies(cookies_to_order, cookies_to_delete, dateOrder, client, stock).getClass());
        assertEquals(0, cookies_to_order.size());
    }

    @Test(expected = Exception.class)
    public void deleteCookiesTest30MinBefore() throws Exception {
        LocalDateTime mockDate = LocalDateTime.parse("2017-12-01 16:30", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        LocalDateTime dateOrder = LocalDateTime.parse("2017-12-01 17:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        PowerMockito.mockStatic(LocalDateTime.class);
        when(LocalDateTime.now()).thenReturn(mockDate);

        Map<Orderable, Integer> cookies_to_delete = new HashMap<>();
        Orderable cookie1 = new Cookie.CookieBuilder()
                .withCooking(Cooking.CHEWY)
                .withDough(Dough.OATMEAL)
                .withFlavour(Flavour.VANILLA)
                .withMix(Mix.MIXED).build();
        cookies_to_delete.put(cookie1, 50);


        stateOrder.deleteCookies(cookies_to_order, cookies_to_delete, dateOrder, client, stock);
    }

    @Test(expected = Exception.class)
    public void validateTest() throws Exception {
        stateOrder.validate();
    }

    @Test(expected = Exception.class)
    public void checkHourTest() throws Exception {
        stateOrder.checkHour(cookies_to_order, stock);
    }

    @Test
    public void cancelOrderCommand() throws Exception {
        LocalDateTime mockDate = LocalDateTime.parse("2017-12-01 14:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        LocalDateTime dateOrder = LocalDateTime.parse("2017-12-01 17:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        PowerMockito.mockStatic(LocalDateTime.class);
        when(LocalDateTime.now()).thenReturn(mockDate);

        assertEquals(OrderCanceled.class, stateOrder.cancelOrder(cookies_to_order, dateOrder, client, stock).getClass());
    }

    @Test(expected = Exception.class)
    public void cancelOrderCommand30MinBefore() throws Exception {
        LocalDateTime mockDate = LocalDateTime.parse("2017-12-01 16:30", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        LocalDateTime dateOrder = LocalDateTime.parse("2017-12-01 17:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        PowerMockito.mockStatic(LocalDateTime.class);
        when(LocalDateTime.now()).thenReturn(mockDate);

        assertEquals(OrderCanceled.class, stateOrder.cancelOrder(cookies_to_order, dateOrder, client, stock).getClass());
    }

}
