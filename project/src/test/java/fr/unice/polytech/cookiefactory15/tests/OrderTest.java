package fr.unice.polytech.cookiefactory15.tests;

import fr.unice.polytech.cookiefactory15.*;
import fr.unice.polytech.cookiefactory15.cookieparts.*;
import fr.unice.polytech.cookiefactory15.customers.Customer;
import fr.unice.polytech.cookiefactory15.orderables.Cookie;
import fr.unice.polytech.cookiefactory15.orderables.Orderable;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrderTest {

    private Order order;
    private Customer customer;
    private TheCookieFactory tcf = TheCookieFactory.get_instance();
    private Map<Orderable, Integer> cookiesToOrder;

    @Before
    public void setUp() {
        customer = new Customer("Marcel");
        TheCookieFactory.get_instance().reset();
        setUpCookiesToOrder();
    }

    private void setUpCookiesToOrder() {
        cookiesToOrder = new HashMap<>();
        Cookie cookieOne = new Cookie.CookieBuilder()
                .withName("Soooo WTF")
                .withCooking(Cooking.CRUNCHY)
                .withDough(Dough.PLAIN)
                .withFlavour(Flavour.VANILLA)
                .withMix(Mix.MIXED)
                .withTopping(Topping.MILK_CHOCO)
                .build();
        Cookie cookieTwo = new Cookie.CookieBuilder()
                .withName("Dark Temptation")
                .withCooking(Cooking.CHEWY)
                .withDough(Dough.OATMEAL)
                .withFlavour(Flavour.CHILI)
                .withMix(Mix.TOPPED)
                .withTopping(Topping.M_AND_M)
                .build();
        Cookie cookieThree = new Cookie.CookieBuilder()
                .withName("Chocotrololo")
                .withCooking(Cooking.CRUNCHY)
                .withDough(Dough.CHOCO)
                .withFlavour(Flavour.CINNAMON)
                .withMix(Mix.MIXED)
                .withTopping(Topping.REESE_BUTTERCUP)
                .build();
        cookiesToOrder.put(cookieOne, 12);
        cookiesToOrder.put(cookieTwo, 5);
        cookiesToOrder.put(cookieThree, 13);
    }

    @After
    public void tearDown() {

    }

    @Test
    public void testDTPrice() throws Exception {
        order = new Order(customer, new Shop("Junit Test"),
                LocalDateTime.of(2017, 12, 25, 10, 45),
                cookiesToOrder, PaymentMode.AE);
        double dtPrice = Math.floor(order.getDutyFreePrice() * 100) / 100;
        assertEquals(94.46, dtPrice, 0.01);
    }

    @Ignore
    public void testOrderWithCustomerBeingMember() throws Exception {
        tcf.addCustomer(customer);
        tcf.createAccount(customer);
        order = new Order(customer,  new Shop("Junit Test"),
                LocalDateTime.of(2017, 12, 25, 10, 45),
                cookiesToOrder, PaymentMode.CB);
        double actualPrice = Math.floor(order.getPrice() * 100) / 100;
        assertEquals(88.79, actualPrice);
    }

    @Ignore
    @DisplayName("when the order sheet is print, check if every variable part of the output is well formed")
    public void testOrderSheetDetails() throws Exception {
        order = new Order(customer, new Shop("Junit Test"),
                LocalDateTime.of(2017, 12, 25, 10, 45),
                cookiesToOrder, PaymentMode.AE);
        String orderSheet = order.orderSheet();
        //Order details
        assertTrue(orderSheet.contains("Soooo WTF x12\t\t25.20$"));
        assertTrue(orderSheet.contains("Chocotrololo x13\t\t32.50$"));
        assertTrue(orderSheet.contains("Dark Temptation x5\t\t14.00$"));
        //Prices
        assertTrue(orderSheet.contains("Total duty free:\t\t71.70$"));
        assertTrue(orderSheet.contains("Total:\t\t\t\t\t81.02$"));
        //Pickup date
        assertTrue(orderSheet.contains("for the 2017-12-25 at 10:45"));
        //Payment mode
        assertTrue(orderSheet.contains("Payment Mode: American Express (+0.12% on total price order)"));
        //Customer details
        assertTrue(orderSheet.contains("Customer details: Marcel"));
    }

}
