package fr.unice.polytech.cookiefactory15.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import fr.unice.polytech.cookiefactory15.TheCookieFactory;
import fr.unice.polytech.cookiefactory15.customers.Customer;

import static org.junit.jupiter.api.Assertions.*;

public class UserAccountStepdefs {

    private TheCookieFactory tcf;
    private Customer customer;
    private Customer anotherCustomer;

    @Given("^a customer named \"([^\"]*)\"$")
    public void aCustomerNamed(String arg0) throws Throwable {
        TheCookieFactory.get_instance().reset();
        tcf = TheCookieFactory.get_instance();
        customer = new Customer(arg0);
        anotherCustomer = new Customer(arg0);
        tcf.addCustomer(customer);
        tcf.addCustomer(anotherCustomer);
        tcf.createAccount(anotherCustomer);
    }

    @When("^the customer \"([^\"]*)\" wants to create an account$")
    public void theCustomerWantsToCreateAnAccount(String arg0) throws Throwable {
        tcf.createAccount(customer);
    }

    @Then("^\"([^\"]*)\" has an account$")
    public void hasAnAccount(String arg0) throws Throwable {
        assertTrue(tcf.hasAccount(customer.getID()));
    }

    @And("^his customer ID is (\\d+)$")
    public void hisCustomerIDIs(int arg0) throws Throwable {
        assertEquals(1, tcf.getCustomerByID(arg0).get().getID());
    }

    @When("^the customer \"([^\"]*)\" with customer ID (\\d+) wants to become a premium member$")
    public void theCustomerWantsToBecomeAPremiumMember(String arg0, int arg1) throws Throwable {
        if (tcf.getAccountByID(arg1).isPresent()) {
            tcf.getAccountByID(arg1).get().becomePremium();
        }
    }

    @Then("^\"([^\"]*)\" with customer ID (\\d+) is a premium member$")
    public void isAPremiumMember(String arg0, int arg1) throws Throwable {
        assertTrue(tcf.getAccountByID(arg1).get().isPremium());
    }

    @When("^the customer \"([^\"]*)\" with customer ID (\\d+) wants to cancel the premium benefit$")
    public void theCustomerWantsToCancelThePremiumBenefit(String arg0, int arg1) throws Throwable {
        if (tcf.getAccountByID(arg1).isPresent()) {
            tcf.getAccountByID(arg1).get().cancelPremium();
        }
    }

    @Then("^\"([^\"]*)\" with customer ID (\\d+) is not a premium member anymore$")
    public void isNotAPremiumMemberAnymore(String arg0, int arg1) throws Throwable {
        assertFalse(tcf.getAccountByID(arg1).get().isPremium());
    }

}
