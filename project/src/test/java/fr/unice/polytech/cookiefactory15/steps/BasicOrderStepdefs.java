package fr.unice.polytech.cookiefactory15.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import fr.unice.polytech.cookiefactory15.*;
import fr.unice.polytech.cookiefactory15.cookieparts.*;
import fr.unice.polytech.cookiefactory15.customers.Customer;
import fr.unice.polytech.cookiefactory15.orderables.Cookie;
import fr.unice.polytech.cookiefactory15.orderables.Orderable;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BasicOrderStepdefs {

    private TheCookieFactory tcf = TheCookieFactory.get_instance();

    @Given("^a customer named \"([^\"]*)\" ordering at shop \"([^\"]*)\" located in \"([^\"]*)\"$")
    public void aCustomerNamedOrderingAtShopLocatedIn(String arg0, String arg1, String arg2) throws Throwable {
        TheCookieFactory.get_instance().reset(); // we reset tcf so that each test is isolated
        Customer customer = new Customer(arg0);
        Shop shop = new Shop(arg1, "Example address", State.COLORADO, LocalTime.of(8, 0), LocalTime.of(18, 0), new ArrayList<>());
        shop.getStock().addToDough(Dough.PLAIN, 200);
        shop.getStock().addToFlavour(Flavour.VANILLA, 200);
        shop.getStock().addToTopping(Topping.MILK_CHOCO, 200);
        tcf.addShop(shop);
        tcf.addCustomer(customer);
    }

    @And("^\"([^\"]*)\" become member$")
    public void becomeMember(String arg0) {
        if (tcf.isCustomer(1)) {
            tcf.createAccount(tcf.getCustomerByID(1).get());
        }
    }

    @When("^\"([^\"]*)\" orders (\\d+) cookies \"([^\"]*)\" at \"([^\"]*)\" for \"([^\"]*)\" on Monday (\\d+)$")
    public void ordersCookiesAtFor(String arg0, int arg1, String arg2, String arg3, String arg4, int arg5) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        tcf.addCookieRecipe(
                new Cookie.CookieBuilder()
                        .withName(arg2)
                        .withCooking(Cooking.CRUNCHY)
                        .withDough(Dough.PLAIN)
                        .withFlavour(Flavour.VANILLA)
                        .withMix(Mix.MIXED)
                        .withTopping(Topping.MILK_CHOCO)
                        .build());

        String[] time = arg4.split(":");
        int hours = Integer.parseInt(time[0]);
        int minutes = Integer.parseInt(time[1]);

        Map<Orderable, Integer> cookies_to_order = new HashMap<>();

        if (tcf.getCookieRecipeByName(arg2).isPresent()) {
            cookies_to_order.put(tcf.getCookieRecipeByName(arg2).get(), arg1);
        }

        if (tcf.isCustomer(1) && tcf.getShopByName(arg3).isPresent()) {
            tcf.order(tcf.getCustomerByID(1).get(), tcf.getShopByName(arg3).get(), arg5,
                    hours, minutes, cookies_to_order, PaymentMode.PAYPAL);
        }
        for (Order o : tcf.getShopByName(arg3).get().getOrders()) {
            if (o.getCookie_to_order().containsValue(arg1))
                o.pay();
        }
    }

    @Then("^the stock of shop \"([^\"]*)\" has now (\\d+) \"([^\"]*)\" ingredients$")
    public void theStockOfShopHasIngredientsLess(String arg0, int arg1, String arg2) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

        if (tcf.getShopByName(arg0).isPresent()) {
            assertEquals(arg1, tcf.getShopByName(arg0).get().getStock().getDoughIngredientAmount(Dough.PLAIN));
            assertEquals(arg1, tcf.getShopByName(arg0).get().getStock().getFlavourIngredientAmount(Flavour.VANILLA));
            assertEquals(arg1, tcf.getShopByName(arg0).get().getStock().getToppingIngredientAmount(Topping.MILK_CHOCO));
        }
    }

    @And("^the price of the order number (\\d+) in shop \"([^\"]*)\" is \"([^\"]*)\"$")
    public void thePriceOfTheOrderInShopIs(int arg0, String arg1, String arg2) throws Throwable {
        if (tcf.getShopByName(arg1).isPresent()) {
            List<Order> order = tcf.getShopByName(arg1).get().getOrders();
            double price = Math.floor(order.get(arg0).getPrice() * 100) / 100;
            assertEquals(Double.parseDouble(arg2), price, 0.01);
        }
    }

    @And("^the statistics of the shop \"([^\"]*)\" show that one order have been ordered for \"([^\"]*)\"$")
    public void theStatisticsOfTheShopShowThatOneOrderHaveBeenOrderedFor(String arg0, String arg1) throws Throwable {
        if (tcf.getShopByName(arg0).isPresent()) {
            assertEquals(1, tcf.getShopByName(arg0).get().getStats().getOrderByHour(arg1));
        }
    }

}
