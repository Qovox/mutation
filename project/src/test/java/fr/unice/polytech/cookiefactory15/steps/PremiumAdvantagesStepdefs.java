package fr.unice.polytech.cookiefactory15.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import fr.unice.polytech.cookiefactory15.Order;
import fr.unice.polytech.cookiefactory15.PaymentMode;
import fr.unice.polytech.cookiefactory15.Shop;
import fr.unice.polytech.cookiefactory15.TheCookieFactory;
import fr.unice.polytech.cookiefactory15.cookieparts.*;
import fr.unice.polytech.cookiefactory15.customers.Customer;
import fr.unice.polytech.cookiefactory15.orderables.Cookie;
import fr.unice.polytech.cookiefactory15.orderables.Orderable;

import java.util.HashMap;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class PremiumAdvantagesStepdefs {

    private Customer customer;
    private TheCookieFactory tcf;
    private Orderable cookie;

    @Given("^a customer named \"([^\"]*)\" create an account$")
    public void aCustomerNamedCreateAnAccount(String arg0) throws Throwable {
        TheCookieFactory.get_instance().reset();
        tcf = TheCookieFactory.get_instance();
        customer = new Customer(arg0);
        customer.createAccount();
    }

    @When("^\"([^\"]*)\" become premium$")
    public void becomePremium(String arg0) throws Throwable {
        customer.getCustomerAccount().becomePremium();
    }

    @And("^\"([^\"]*)\" orders (\\d+) cookies at \"([^\"]*)\"$")
    public void orderCookies(String arg0, int arg1, String arg2) throws Throwable {
        Shop shop = new Shop(arg2);
        shop.getStock().addToTopping(Topping.WHITE_CHOCO, 100);
        shop.getStock().addToFlavour(Flavour.VANILLA, 100);
        shop.getStock().addToDough(Dough.OATMEAL, 100);
        tcf.addShop(shop);

        Map<Orderable, Integer> cookieToOrder = new HashMap<>();
        cookie = new Cookie.CookieBuilder()
                .withCooking(Cooking.CRUNCHY)
                .withDough(Dough.OATMEAL)
                .withFlavour(Flavour.VANILLA)
                .withMix(Mix.TOPPED)
                .withTopping(Topping.WHITE_CHOCO).build();
        cookieToOrder.put(cookie, arg1);

        tcf.order(customer, shop, 21, 23, 59, cookieToOrder, PaymentMode.PAYPAL);
    }

    @Then("^the basket of \"([^\"]*)\" is saved$")
    public void theBasketOfIsSaved(String arg0) throws Throwable {
        Shop shop = tcf.getShopByName("ShopCookies").get();
        assertTrue(shop.getStock().getDoughIngredientAmount(Dough.OATMEAL) != 100);

    }


    @And("^\"([^\"]*)\" delete (\\d+) cookies$")
    public void deleteCookies(String arg0, int arg1) throws Throwable {
        Order order = tcf.getShopByName("ShopCookies").get().getOrders().get(0);
        Map<Orderable, Integer> cookiestodelete = new HashMap<>();
        cookiestodelete.put(cookie, arg1);
        order.deleteCookies(cookiestodelete);
    }

    @Then("^\"([^\"]*)\" has (\\d+) cookies in his basket$")
    public void hasCookiesInHisBasket(String arg0, int arg1) throws Throwable {
        Order order = tcf.getShopByName("ShopCookies").get().getOrders().get(0);
        for (Orderable o : order.getCookie_to_order().keySet()) {
            assertEquals(arg1, order.getCookie_to_order().get(o).intValue());
        }
    }

}
