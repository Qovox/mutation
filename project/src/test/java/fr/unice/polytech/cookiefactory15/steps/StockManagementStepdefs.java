package fr.unice.polytech.cookiefactory15.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import fr.unice.polytech.cookiefactory15.Shop;
import fr.unice.polytech.cookiefactory15.State;
import fr.unice.polytech.cookiefactory15.cookieparts.*;
import fr.unice.polytech.cookiefactory15.orderables.Cookie;
import fr.unice.polytech.cookiefactory15.orderables.Orderable;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class StockManagementStepdefs {

    private Shop shop;
    private Map<Orderable, Integer> cookieToOrder;

    @Given("^a new Shop named \"([^\"]*)\" opening with a certain stock$")
    public void aNewShopNamedOpeningWithACertainStock(String arg0) throws Throwable {
        shop = new Shop(arg0, "Example address", State.COLORADO, LocalTime.of(8, 0), LocalTime.of(18, 0), new ArrayList<>());
        shop.getStock().addToDough(Dough.OATMEAL, 10);
        shop.getStock().addToFlavour(Flavour.VANILLA, 10);
        shop.getStock().addToTopping(Topping.WHITE_CHOCO, 10);
    }

    @When("^the shop named \"([^\"]*)\" recieves an order of (\\d+) cookies$")
    public void theShopNamedRecievesAnOrderOfCookies(String arg0, int arg1) {
        cookieToOrder = new HashMap<>();
        cookieToOrder.put(new Cookie.CookieBuilder()
                .withCooking(Cooking.CRUNCHY)
                .withDough(Dough.OATMEAL)
                .withFlavour(Flavour.VANILLA)
                .withMix(Mix.TOPPED)
                .withTopping(Topping.WHITE_CHOCO).build(), arg1);
    }

    @Then("^the stock can assure the order$")
    public void theStockCanAssureTheOrder() {
        for (Orderable o : cookieToOrder.keySet())
            assertTrue(shop.getStock().isDoable(o, cookieToOrder.get(o)));
    }

    @Then("^the stock cannot assure the order$")
    public void theStockCannotAssureTheOrder() throws Throwable {
        for (Orderable o : cookieToOrder.keySet())
            assertFalse(shop.getStock().isDoable(o, cookieToOrder.get(o)));
    }

}
