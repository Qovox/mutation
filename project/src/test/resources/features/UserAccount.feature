Feature: User account

  Background:
    Given a customer named "Marcel"

  Scenario: create an account for Marcel
    When the customer "Marcel" wants to create an account
    Then "Marcel" has an account
    And his customer ID is 1

  Scenario: become premium
    When the customer "Marcel" with customer ID 2 wants to become a premium member
    Then "Marcel" with customer ID 2 is a premium member

  Scenario: cancel premium
    When the customer "Marcel" with customer ID 2 wants to cancel the premium benefit
    Then "Marcel" with customer ID 2 is not a premium member anymore