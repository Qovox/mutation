Feature: Basic order

  Background:
    Given a customer named "Marcel" ordering at shop "CookieFactory" located in "State"

  Scenario: order
    When "Marcel" orders 3 cookies "SOOOO Choco" at "CookieFactory" for "16:30" on Monday 21
    Then the stock of shop "CookieFactory" has now 197 "PLAIN" ingredients
    And the stock of shop "CookieFactory" has now 197 "VANILLA" ingredients
    And the stock of shop "CookieFactory" has now 197 "MIXED" ingredients
    And the stock of shop "CookieFactory" has now 197 "MILK_CHOCO" ingredients
    And the price of the order number 0 in shop "CookieFactory" is "8.61"
    And the statistics of the shop "CookieFactory" show that one order have been ordered for "16:30"

  Scenario: order more than 30 cookies
    When "Marcel" become member
    And "Marcel" orders 30 cookies "SOOOO Choco" at "CookieFactory" for "13:30" on Monday 21
    Then the price of the order number 0 in shop "CookieFactory" is "84.56"

  Scenario: apply discount
    When "Marcel" become member
    And "Marcel" orders 30 cookies "SOOOO Choco" at "CookieFactory" for "13:30" on Monday 21
    And "Marcel" orders 6 cookies "SOOOO Choco" at "CookieFactory" for "14:30" on Monday 21
    Then the price of the order number 1 in shop "CookieFactory" is "15.66"