package fr.unice.polytech.cookiefactory15;

public class Manager {

    private String name;

    public Manager(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}