package fr.unice.polytech.cookiefactory15.orderables;

import fr.unice.polytech.cookiefactory15.cookieparts.Dough;
import fr.unice.polytech.cookiefactory15.cookieparts.Flavour;
import fr.unice.polytech.cookiefactory15.cookieparts.Mix;
import fr.unice.polytech.cookiefactory15.cookieparts.Topping;

import java.util.EnumMap;
import java.util.Map;

public class CookieBox extends Orderable {

    private double price;
    private int nbCookies;
    private Map<Cookie, Integer> boxContent;

    public CookieBox(String name, double price, Map<Cookie, Integer> boxContent) {
        super(name);
        this.price = price;
        this.boxContent = boxContent;
        calculateNbCookies();
    }

    @Override
    public double calculateTotalPrice() {
        return price;
    }

    @Override
    public Map<Dough, Integer> getDoughToMap() {
        Map<Dough, Integer> res = new EnumMap<>(Dough.class);

        for (Cookie cookie : this.boxContent.keySet()) {
            res.putAll(cookie.getDoughToMap());
        }

        return res;
    }

    @Override
    public Map<Flavour, Integer> getFlavourToMap() {
        Map<Flavour, Integer> res = new EnumMap<>(Flavour.class);

        for (Cookie cookie : this.boxContent.keySet()) {
            res.putAll(cookie.getFlavourToMap());
        }

        return res;
    }

    @Override
    public Map<Mix, Integer> getMixToMap() {
        Map<Mix, Integer> res = new EnumMap<>(Mix.class);

        for (Cookie cookie : this.boxContent.keySet()) {
            res.putAll(cookie.getMixToMap());
        }

        return res;
    }

    @Override
    public Map<Topping, Integer> getToppingToMap() {
        Map<Topping, Integer> res = new EnumMap<>(Topping.class);

        for (Cookie cookie : this.boxContent.keySet()) {
            res.putAll(cookie.getToppingToMap());
        }

        return res;
    }

    private void calculateNbCookies() {
        for (Map.Entry cookie : boxContent.entrySet()) {
            nbCookies += boxContent.get(cookie.getKey());
        }
    }

    @Override
    public int getNumberInOrderable() {
        return nbCookies;
    }

}
