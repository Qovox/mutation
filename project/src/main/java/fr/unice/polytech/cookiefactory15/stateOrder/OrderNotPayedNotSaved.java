package fr.unice.polytech.cookiefactory15.stateOrder;

import fr.unice.polytech.cookiefactory15.Stock;
import fr.unice.polytech.cookiefactory15.customers.Customer;
import fr.unice.polytech.cookiefactory15.orderables.Orderable;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

public class OrderNotPayedNotSaved implements StateOrder {

    @Override
    public StateOrder pay(Map<Orderable, Integer> cookieOrdered, Stock stock) throws Exception {
        for (Orderable o : cookieOrdered.keySet()) {
            if (!stock.isDoable(o, cookieOrdered.get(o))) throw new Exception();
        }
        for (Orderable o : cookieOrdered.keySet()) {
            stock.removeIngredientsFromCookie(o, cookieOrdered.get(o));
        }
        return new OrderPayedSaved();
    }

    @Override
    public StateOrder validate() throws Exception {
        throw new Exception();

    }

    @Override
    public StateOrder checkHour(Map<Orderable, Integer> cookieOrdered, Stock stock) throws Exception {
        throw new Exception();
    }

    @Override
    public StateOrder deleteCookies(Map<Orderable, Integer> cookieOrdered, Map<Orderable, Integer> cookiesToDelete, LocalDateTime date, Customer client, Stock stock) throws Exception {
        if (client.hasAccount() && client.getCustomerAccount().isPremium()) {
            if (LocalDateTime.now().isBefore(date.minusHours(1))) {
                Set<Orderable> orderableSet = cookiesToDelete.keySet();

                for (Orderable o : orderableSet) {
                    if (cookiesToDelete.containsKey(o) && cookieOrdered.containsKey(o)) {
                        cookieOrdered.put(o, cookieOrdered.get(o) - cookiesToDelete.get(o));
                        if (cookieOrdered.get(o) <= 0)
                            cookieOrdered.remove(o);
                    }
                }
                if (cookieOrdered.size() == 0) return new OrderCanceled();

                return this;
            }
        }
        throw new Exception();
    }

    @Override
    public StateOrder cancelOrder(Map<Orderable, Integer> cookieOrdered, LocalDateTime date, Customer client, Stock stock) throws Exception {
        if (client.hasAccount() && client.getCustomerAccount().isPremium()) {
            if (LocalDateTime.now().isBefore(date.minusHours(1))) {
                cookieOrdered.clear();
                return new OrderCanceled();
            }
        }
        throw new Exception();
    }

}
