package fr.unice.polytech.cookiefactory15.cookieparts;

public enum Dough {

    PLAIN(1, 0.01),
    CHOCO(1.1, 0.02),
    PEANUT_BUTTER(1.2, 0.2),
    OATMEAL(1.3, 0.25);

    private double price;
    private double margin;

    Dough(double price, double margin) {
        this.price = price;
        this.margin = margin;
    }

    public double getMargin() {
        return margin;
    }

    public double getPrice() {
        return price * (1 + margin);
    }

    public void setMargin(double margin) {
        this.margin = margin;
    }

}