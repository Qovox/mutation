
package fr.unice.polytech.cookiefactory15.pricestrategy;

import fr.unice.polytech.cookiefactory15.TheCookieFactory;
import fr.unice.polytech.cookiefactory15.orderables.Orderable;

import java.time.LocalTime;
import java.util.Map;

abstract class BasicPriceStrategy {

    private double stateTax;
    private Map<Orderable, Integer> cookie_to_order;
    private double paymentModeModifier;
    private LocalTime closingTime;
    private int nbCookies;

    BasicPriceStrategy(double stateTax, Map<Orderable, Integer> cookie_to_order, double paymentModeModifier, LocalTime closingTime) {
        this.stateTax = stateTax;
        this.cookie_to_order = cookie_to_order;
        this.paymentModeModifier = paymentModeModifier;
        this.closingTime = closingTime;
        nbCookies = 0;
    }

    double calculateBasicDTPrice() {
        double dtPrice = 0;
        Orderable currentCookie;

        for (Map.Entry cookie : cookie_to_order.entrySet()) {
            currentCookie = (Orderable) cookie.getKey();
            dtPrice += currentCookie.calculateTotalPrice() * cookie_to_order.get(currentCookie);

            nbCookies += cookie_to_order.get(currentCookie) * currentCookie.getNumberInOrderable();
        }

        return dtPrice;
    }

    double calculateBasicTaxes(double dtPrice) {
        return dtPrice * stateTax;
    }

    double calculateBasicDiscount() {
        double discount = paymentModeModifier;
        LocalTime now = LocalTime.now();
        if (now.isAfter(closingTime.minusHours(1)) && now.isBefore(closingTime)) {
            discount += TheCookieFactory.LAST_HOUR_DISCOUNT;
        }

        return discount;
    }

    int getNbCookies() {
        return nbCookies;
    }

}
