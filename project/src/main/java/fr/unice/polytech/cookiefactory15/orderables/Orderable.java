package fr.unice.polytech.cookiefactory15.orderables;

import fr.unice.polytech.cookiefactory15.cookieparts.Dough;
import fr.unice.polytech.cookiefactory15.cookieparts.Flavour;
import fr.unice.polytech.cookiefactory15.cookieparts.Mix;
import fr.unice.polytech.cookiefactory15.cookieparts.Topping;

import java.util.Map;

public abstract class Orderable {

    private String name;

    private static final int defaultNumber = 1;

    public Orderable(String name) {
        this.name = name;
    }

    public abstract double calculateTotalPrice();

    public String getName() {
        return name;
    }

    public abstract Map<Dough, Integer> getDoughToMap();

    public abstract Map<Mix, Integer> getMixToMap();

    public abstract Map<Topping, Integer> getToppingToMap();

    public abstract Map<Flavour, Integer> getFlavourToMap();

    public int getNumberInOrderable() {
        return defaultNumber;
    }

}
