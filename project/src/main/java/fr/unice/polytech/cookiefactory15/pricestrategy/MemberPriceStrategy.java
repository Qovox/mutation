package fr.unice.polytech.cookiefactory15.pricestrategy;

import fr.unice.polytech.cookiefactory15.TheCookieFactory;
import fr.unice.polytech.cookiefactory15.customers.Customer;
import fr.unice.polytech.cookiefactory15.orderables.Orderable;

import java.time.LocalTime;
import java.util.Map;

public class MemberPriceStrategy extends BasicPriceStrategy implements CalcultatePriceStrategy {

    private Customer customer;

    public MemberPriceStrategy(Customer customer, double stateTax, Map<Orderable, Integer> cookies_to_order,
                               double paymentModeModifier, LocalTime closingTime) {
        super(stateTax, cookies_to_order, paymentModeModifier, closingTime);
        this.customer = customer;
    }

    @Override
    public double calculatePrice() {
        double priceDT = calculateDTPrice();
        double priceWithTax;
        double finalPrice;
        double discount = super.calculateBasicDiscount();
        discount += calculateAdvantage(super.getNbCookies());
        if (customer.getCustomerAccount().isEligible(super.getNbCookies())) {
            discount += 0.1;
        }
        priceWithTax = priceDT + calculateBasicTaxes(priceDT);
        finalPrice = priceWithTax - (priceDT * discount) - customer.getCustomerAccount().lastVoucher();
        if (finalPrice < 0) {
            return 0;
        }
        return finalPrice;
    }

    @Override
    public double calculateDTPrice() {
        return super.calculateBasicDTPrice();
    }

    private double calculateAdvantage(int nbCookies) {
        double seniorityAdv = (double) customer.getCustomerAccount().getSeniority();
        double quantityAdv = (double) (nbCookies / TheCookieFactory.NB_NEEDED_PROGRESSIVE) / 100.0;

        return seniorityAdv > quantityAdv ? seniorityAdv : quantityAdv;
    }


}
