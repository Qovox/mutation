package fr.unice.polytech.cookiefactory15.cookieparts;

public enum Topping {

    WHITE_CHOCO(0.3, 0.12),
    MILK_CHOCO(0.4, 0.2),
    M_AND_M(0.5, 0.18),
    REESE_BUTTERCUP(0.6, 0.14);

    private double price;
    private double margin;

    Topping(double price, double margin) {
        this.price = price;
        this.margin = margin;
    }

    public double getMargin() {
        return margin;
    }

    public double getPrice() {
        return price * (1 + margin);
    }

    public void setMargin(double margin) {
        this.margin = margin;
    }

}