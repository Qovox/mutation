package fr.unice.polytech.cookiefactory15.stateOrder;

import fr.unice.polytech.cookiefactory15.Stock;
import fr.unice.polytech.cookiefactory15.customers.Customer;
import fr.unice.polytech.cookiefactory15.customers.CustomerAccount;
import fr.unice.polytech.cookiefactory15.orderables.Orderable;

import java.time.LocalDateTime;
import java.util.Map;

public interface StateOrder {

    StateOrder pay(Map<Orderable, Integer> cookieOrdered, Stock stock) throws Exception;

    StateOrder validate() throws Exception;

    /**
     * @param cookieOrdered the order content
     * @param stock the stock of the shop where the customer ordered
     * @return a new state for the order
     * @throws Exception Méthode à appeler apres 4h afin de rendre le panier à l'état " non garanti"
     */
    StateOrder checkHour(Map<Orderable, Integer> cookieOrdered, Stock stock) throws Exception;

    StateOrder deleteCookies(Map<Orderable, Integer> cookieOrdered, Map<Orderable, Integer> cookiesToDelete, LocalDateTime date, Customer client, Stock stock) throws Exception;

    StateOrder cancelOrder(Map<Orderable, Integer> cookieOrdered, LocalDateTime date, Customer client, Stock stock) throws Exception;

}