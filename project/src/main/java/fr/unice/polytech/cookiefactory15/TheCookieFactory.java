package fr.unice.polytech.cookiefactory15;

import fr.unice.polytech.cookiefactory15.customers.Customer;
import fr.unice.polytech.cookiefactory15.customers.CustomerAccount;
import fr.unice.polytech.cookiefactory15.orderables.Cookie;
import fr.unice.polytech.cookiefactory15.orderables.Orderable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

public class TheCookieFactory {

    private static TheCookieFactory INSTANCE = new TheCookieFactory();

    public static final int COOKIE_DISCOUNT = 30;
    public static final int NB_NEEDED_PROGRESSIVE = 25;
    public static final double LAST_HOUR_DISCOUNT = 0.05;
    public static final int VOUCHER_MONTH_LIFESPAN = 2;
    public static double deltaPrice = 0.05;
    public static int nextID = 1;

    private Collection<Shop> shops;
    private Collection<Cookie> cookies_recipes;
    private Collection<Customer> customers;
    private Collection<CustomerAccount> accounts;

    private TheCookieFactory() { //private to ensure there's only one instance
        shops = new ArrayList<>();
        cookies_recipes = new ArrayList<>();
        customers = new ArrayList<>();
        accounts = new ArrayList<>();
    }

    public static TheCookieFactory get_instance() {
        return INSTANCE;
    }

    public void reset() {
        nextID = 1;
        shops.clear();
        cookies_recipes.clear();
        customers.clear();
        accounts.clear();
    }

    public void addShop(Shop shop) {
        shops.add(shop);
    }

    public void addCookieRecipe(Cookie cookie) {
        cookies_recipes.add(cookie);
    }

    public void removeCookieRecipe(Cookie cookie) {
        cookies_recipes.remove(cookie);
    }

    public boolean isAKnownRecipe(Cookie cookie) {
        for (Cookie known_cookie : cookies_recipes) {
            if (known_cookie.equals(cookie)) return true;
        }
        return false;
    }

    public void addCustomer(Customer customer) {
        if (!customers.contains(customer)) {
            customers.add(customer);
        }
    }

    public void createAccount(Customer customer) {
        if (!hasAccount(customer.getID())) {
            customer.createAccount();
            accounts.add(customer.getCustomerAccount());
        }
    }

    public void order(Customer customer, Shop shop, int day, int hours, int minutes,
                      Map<Orderable, Integer> cookie_to_order, PaymentMode paymentMode) throws Exception {
        addCustomer(customer);
        shop.order(customer, createAppointment(day, hours, minutes), cookie_to_order, paymentMode);
    }

    public Optional<Shop> getShopByName(String shopName) {
        for (Shop shop : shops) {
            if (shop.getShopName().equals(shopName)) return Optional.of(shop);
        }
        return Optional.empty();
    }

    public Optional<Customer> getCustomerByID(int wantedID) {
        for (Customer customer : customers) {
            if (customer.getID() == wantedID) {
                return Optional.of(customer);
            }
        }
        return Optional.empty();
    }

    public Optional<CustomerAccount> getAccountByID(int wantedID) {
        Optional<Customer> customer = getCustomerByID(wantedID);
        if (customer.isPresent() && customer.get().hasAccount()) {
            return Optional.of(customer.get().getCustomerAccount());
        }
        return Optional.empty();
    }

    public Optional<Cookie> getCookieRecipeByName(String name) {
        for (Cookie cookie : cookies_recipes) {
            if (cookie.getName().equals(name)) {
                return Optional.of(cookie);
            }
        }
        return Optional.empty();
    }

    public LocalDateTime createAppointment(int day, int hours, int minutes) {
        int year = LocalDate.now().getYear();
        int month = LocalDate.now().getMonth().getValue();
        int nowHours = LocalTime.now().getHour();
        if (LocalDate.now().getDayOfMonth() > day) {
            if (month == 12) {
                return LocalDateTime.of(year + 1, 1, day, hours, minutes);
            } else {
                return LocalDateTime.of(year, month + 1, day, hours, minutes);
            }
        } else if (LocalDate.now().getDayOfMonth() == day && (hours - nowHours) < 2) {
            return LocalDateTime.of(year, month, day, nowHours + 2, minutes);
        } else {
            return LocalDateTime.of(year, month, day, hours, minutes);
        }
    }

    public boolean hasAccount(int customerID) {
        Optional<Customer> customer = getCustomerByID(customerID);
        return customer.isPresent() && customer.get().hasAccount();
    }

    public boolean isCustomer(int customerID) {
        for (Customer customer : this.customers) {
            if (customer.getID() == customerID) {
                return true;
            }
        }
        return false;
    }

    public void setCustomCookieMargin(double margin) {
        deltaPrice = margin;
    }

}