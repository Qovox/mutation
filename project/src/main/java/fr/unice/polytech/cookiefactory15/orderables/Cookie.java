package fr.unice.polytech.cookiefactory15.orderables;

import fr.unice.polytech.cookiefactory15.TheCookieFactory;
import fr.unice.polytech.cookiefactory15.cookieparts.*;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;

public class Cookie extends Orderable {

    private Cooking cooking;
    private Dough dough;
    private Optional<Flavour> flavour = Optional.empty();
    private Mix mix;
    private Topping[] topping = new Topping[3];

    private Cookie(CookieBuilder cookieBuilder) {
        super(cookieBuilder.name);
        this.cooking = cookieBuilder.cooking;
        this.dough = cookieBuilder.dough;
        if (cookieBuilder.getFlavour().isPresent()) {
            this.flavour = Optional.of(cookieBuilder.flavour);
        }
        this.mix = cookieBuilder.mix;
        this.topping = cookieBuilder.topping;
    }

    @Override
    public double calculateTotalPrice() {
        double flavourPrice = 0;
        if (this.flavour.isPresent())
            flavourPrice = this.flavour.get().getPrice();
        double totalPrice = this.cooking.getPrice() + this.dough.getPrice() + flavourPrice + this.mix.getPrice() + toppingPrice();

        if (!TheCookieFactory.get_instance().isAKnownRecipe(this)) {
            totalPrice *= (1 + TheCookieFactory.deltaPrice);
        }

        return totalPrice;
    }

    private double toppingPrice() {
        double res = 0;

        for (Topping t : this.topping)
            if (t != null)
                res += t.getPrice();

        return res;
    }

    @Override
    public Map<Dough, Integer> getDoughToMap() {
        Map<Dough, Integer> res = new EnumMap<>(Dough.class);

        res.put(this.dough, 1);

        return res;
    }

    @Override
    public Map<Flavour, Integer> getFlavourToMap() {
        Map<Flavour, Integer> res = new EnumMap<>(Flavour.class);

        this.flavour.ifPresent(f -> res.put(f, 1));

        return res;
    }

    @Override
    public Map<Mix, Integer> getMixToMap() {
        Map<Mix, Integer> res = new EnumMap<>(Mix.class);

        res.put(this.mix, 1);

        return res;
    }

    @Override
    public Map<Topping, Integer> getToppingToMap() {
        Map<Topping, Integer> res = new EnumMap<>(Topping.class);

        for (Topping t : this.topping)
            if (t != null)
                res.put(t, 1);

        return res;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cookie)) return false;

        Cookie cookie = (Cookie) o;

        return (cooking == cookie.cooking) &&
                (dough == cookie.dough) &&
                (flavour.equals(cookie.flavour)) &&
                (mix == cookie.mix) &&
                toppingEquals(topping, cookie.topping);

    }

    @Override
    public int hashCode() {
        int result = cooking.hashCode();
        result = 31 * result + dough.hashCode();
        result = 31 * result + flavour.hashCode();
        result = 31 * result + mix.hashCode();
        result = 31 * result + Arrays.hashCode(topping);
        return result;
    }

    private boolean toppingEquals(Topping[] topping, Topping[] toppingToCompare) {
        for (int i = 0; i < 3; i++) {
            if (topping[i] != toppingToCompare[i])
                return false;
        }
        return true;
    }

    public static class CookieBuilder {

        String name;
        Cooking cooking;
        Dough dough;
        Flavour flavour;
        Mix mix;
        Topping[] topping = new Topping[3];
        private int toppingIndex = 0;

        public CookieBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public CookieBuilder withCooking(Cooking cooking) {
            this.cooking = cooking;
            return this;
        }

        public CookieBuilder withDough(Dough dough) {
            this.dough = dough;
            return this;
        }

        public CookieBuilder withFlavour(Flavour flavour) {
            this.flavour = flavour;
            return this;
        }

        public CookieBuilder withMix(Mix mix) {
            this.mix = mix;
            return this;
        }

        public CookieBuilder withTopping(Topping topping) {
            if (toppingIndex < 3) {
                this.topping[toppingIndex] = topping;
                toppingIndex++;
            }
            return this;
        }

        public Cookie build() {
            toppingIndex = 0;
            return new Cookie(this);
        }

        public Optional<Flavour> getFlavour() {
            return Optional.ofNullable(flavour);
        }

    }

}