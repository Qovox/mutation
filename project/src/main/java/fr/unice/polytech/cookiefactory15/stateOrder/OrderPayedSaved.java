package fr.unice.polytech.cookiefactory15.stateOrder;

import fr.unice.polytech.cookiefactory15.Stock;
import fr.unice.polytech.cookiefactory15.cookieparts.Dough;
import fr.unice.polytech.cookiefactory15.cookieparts.Flavour;
import fr.unice.polytech.cookiefactory15.cookieparts.Topping;
import fr.unice.polytech.cookiefactory15.customers.Customer;
import fr.unice.polytech.cookiefactory15.orderables.Orderable;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

public class OrderPayedSaved implements StateOrder {

    @Override
    public StateOrder pay(Map<Orderable, Integer> cookieOrdered, Stock stock) throws Exception {
        throw new Exception();
    }

    @Override
    public StateOrder validate() throws Exception {
        return new OrderValidated();

    }

    @Override
    public StateOrder checkHour(Map<Orderable, Integer> cookieOrdered, Stock stock) throws Exception {
        throw new Exception();
    }

    @Override
    public StateOrder deleteCookies(Map<Orderable, Integer> cookieOrdered, Map<Orderable, Integer> cookiesToDelete, LocalDateTime date, Customer client, Stock stock) throws Exception {
        if (client.hasAccount() && client.getCustomerAccount().isPremium()) {
            if (LocalDateTime.now().isBefore(date.minusHours(1))) {
                Set<Orderable> orderableSet = cookiesToDelete.keySet();
                double discount = 0;

                for (Orderable o : orderableSet) {
                    if (cookiesToDelete.containsKey(o) && cookieOrdered.containsKey(o)) {
                        cookieOrdered.put(o, cookieOrdered.get(o) - cookiesToDelete.get(o));
                        if (cookieOrdered.get(o) <= 0)
                            cookieOrdered.remove(o);
                        else discount += cookiesToDelete.get(o) * o.calculateTotalPrice();

                    }
                }
                resetCookiesInStock(cookiesToDelete, stock);
                if (client.hasAccount())
                    client.getCustomerAccount().addVoucher(LocalDateTime.now(), discount);

                if (cookieOrdered.size() == 0) return new OrderCanceled();

                return this;
            }
        }
        throw new Exception();
    }

    @Override
    public StateOrder cancelOrder(Map<Orderable, Integer> cookieOrdered, LocalDateTime date, Customer client, Stock stock) throws Exception {
        if (client.hasAccount() && client.getCustomerAccount().isPremium()) {
            if (LocalDateTime.now().isBefore(date.minusHours(1))) {
                double discount = 0;
                for (Orderable o : cookieOrdered.keySet()) {
                    discount += cookieOrdered.get(o) * o.calculateTotalPrice();
                }
                resetCookiesInStock(cookieOrdered, stock);
                if (client.hasAccount())
                    client.getCustomerAccount().addVoucher(LocalDateTime.now(), discount);
                cookieOrdered.clear();
                return new OrderCanceled();
            }
        }
        throw new Exception();

    }

    private void resetCookiesInStock(Map<Orderable, Integer> cookieToDelete, Stock stock) {
        for (Orderable o : cookieToDelete.keySet()) {
            for (Dough dough : o.getDoughToMap().keySet()) {
                stock.addToDough(dough, o.getDoughToMap().get(dough) * cookieToDelete.get(o));
            }
            for (Flavour flavour : o.getFlavourToMap().keySet()) {
                stock.addToFlavour(flavour, o.getFlavourToMap().get(flavour) * cookieToDelete.get(o));
            }
            for (Topping topping : o.getToppingToMap().keySet()) {
                stock.addToTopping(topping, o.getToppingToMap().get(topping) * cookieToDelete.get(o));
            }
        }
    }

}
