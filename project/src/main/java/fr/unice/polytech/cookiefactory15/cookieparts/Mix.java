package fr.unice.polytech.cookiefactory15.cookieparts;

public enum Mix {

    MIXED(0.5),
    TOPPED(0.6);

    private double price;

    Mix(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

}