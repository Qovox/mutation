package fr.unice.polytech.cookiefactory15;

public enum PaymentMode {

    CB("Credit Card", -0.05),
    PAYPAL("Paypal", 0.05),
    BITCOIN("BitCoin", 0.10),
    AE("American Express", 0.12);

    private String paymentName;
    private double priceShift;

    PaymentMode(String paymentName, double priceShift) {
        this.paymentName = paymentName;
        this.priceShift = priceShift;
    }

    public double getPriceShift() {
        return priceShift;
    }

    @Override
    public String toString() {
        String priceShiftModifier = " (";
        if (priceShift < 0) {
            priceShiftModifier += priceShift + "% on total price order)";
        } else {
            priceShiftModifier += "+" + priceShift + "% on total price order)";
        }
        return paymentName + priceShiftModifier;
    }

}
