package fr.unice.polytech.processors;

import spoon.processing.AbstractProcessor;
import spoon.reflect.code.CtIf;
import spoon.reflect.code.CtStatement;

/**
 * Created by DALLA-NORA ENZO in 2017, for Solar Belle Planète.
 * This code is owned by DALLA-NORA ENZO. All rights reserved.
 */
public class IfSwapProcessor extends AbstractProcessor<CtIf> {

    @Override
    public void process(CtIf element) {
        CtStatement ifBlock = element.getThenStatement();
        CtStatement elseBlock = element.getElseStatement();

        element.setThenStatement(elseBlock);
        element.setElseStatement(ifBlock);
    }

}
