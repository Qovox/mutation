package fr.unice.polytech.processors;

import spoon.processing.AbstractProcessor;
import spoon.reflect.code.CtLiteral;

public class IntegerPlusTenProcessor extends AbstractProcessor<CtLiteral> {

    public boolean isToBeProcessed(CtLiteral element){
        return element.getValue() instanceof Integer;
    }

    @Override
    public void process(CtLiteral element) {
        element.setValue((int) element.getValue() + 10);
    }

}