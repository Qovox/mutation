package fr.unice.polytech.processors;

import spoon.processing.AbstractProcessor;
import spoon.reflect.code.CtLiteral;

/**
 * Created by DALLA-NORA ENZO in 2017, for Solar Belle Planète.
 * This code is owned by DALLA-NORA ENZO. All rights reserved.
 */
public class StringSuppressProcessor extends AbstractProcessor<CtLiteral> {

    public boolean isToBeProcessed(CtLiteral element){
        return element.getValue() instanceof String;
    }

    @Override
    public void process(CtLiteral element) {
        element.setValue("");
    }
}
