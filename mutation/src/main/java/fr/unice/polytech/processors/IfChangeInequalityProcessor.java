package fr.unice.polytech.processors;

import java.util.List;
import spoon.processing.AbstractProcessor;
import spoon.reflect.code.BinaryOperatorKind;
import spoon.reflect.code.CtAssignment;
import spoon.reflect.code.CtBinaryOperator;
import spoon.reflect.code.CtExpression;
import spoon.reflect.code.CtIf;
import spoon.reflect.code.CtUnaryOperator;
import spoon.reflect.code.UnaryOperatorKind;
import spoon.reflect.declaration.CtElement;
import spoon.reflect.visitor.filter.TypeFilter;

public class IfChangeInequalityProcessor extends AbstractProcessor<CtIf> {

    public void process(CtIf element) {

        List<CtBinaryOperator> conditions = element.getCondition().getElements(new TypeFilter<>(CtBinaryOperator.class));

        for (CtBinaryOperator condition : conditions) {

            switch (condition.getKind()) {

                case GT:
                    condition.setKind(BinaryOperatorKind.LT);
                    break;

                case GE:
                    condition.setKind(BinaryOperatorKind.LE);
                    break;

                case LE:
                    condition.setKind(BinaryOperatorKind.GE);
                    break;

                case LT:
                    condition.setKind(BinaryOperatorKind.GT);
                    break;
            }

        }

    }

}
